require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource "Books" do
  header "Accept", "application/json"
  header "Authorization", :basic_auth

  let(:user) { create(:user, name: 'Rafael', role: 'user', username: 'rafael', password: 'password') }
  let(:author) { create(:author, user: user) }
  let!(:book) { create(:book, author: author) }
  let(:basic_auth) do
    ActionController::HttpAuthentication::Basic.encode_credentials(user.username, user.password)
  end
  
  get "/v1/users/:user_id/authors/:author_id/books/" do
    let(:user_id) { user.id }
    let(:author_id) { author.id }
    
    example "Listing books" do
      do_request

      expect(status).to be 200
    end
  end

  get "/v1/users/:user_id/authors/:author_id/books/:id" do
    let(:user_id) { user.id }
    let(:author_id) { author.id }
    let(:id) { book.id }
    
    example "Listing specific book" do
      do_request

      expect(status).to be 200
    end
  end

  delete "/v1/users/:user_id/authors/:author_id/books/:id" do
    let(:user_id) { user.id }
    let(:author_id) { author.id }
    let(:id) { book.id }
    
    example "Deleting a specific book" do
      do_request

      expect(status).to be 204
    end
  end

  put "/v1/users/:user_id/authors/:author_id/books/:id" do
    parameter :name
    parameter :username
    parameter :role

    let(:user_id) { user.id }
    let(:author_id) { author.id }
    let(:id) { book.id }
    
    example "Updating a specific book" do
      do_request(title: 'foobar', author_id: author.id, published_at: Time.current)

      expect(status).to be 204
    end
  end

  post "/v1/users/:user_id/authors/:author_id/books" do
    let(:user_id) { user.id }
    let(:author_id) { author.id }
    
    parameter :title, required: true
    parameter :author_id, required: true
    parameter :published_at, required: true

    example "Creating a book" do
      do_request(title: 'foobar', author_id: author.id, published_at: Time.current)
      
      expect(status).to be 201
    end
  end
end
