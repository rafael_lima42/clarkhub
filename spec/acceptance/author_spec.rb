require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource "Authors" do
  header "Accept", "application/json"
  header "Authorization", :basic_auth

  let(:user) { create(:user, name: 'Rafael', role: 'user', username: 'rafael', password: 'password') }
  let!(:author) { create(:author, user: user) }
  let(:basic_auth) do
    ActionController::HttpAuthentication::Basic.encode_credentials(user.username, user.password)
  end
  
  get "/v1/users/:user_id/authors/" do
    let(:user_id) { user.id }
    
    example "Listing authors" do
      do_request

      expect(response_body).to eq [{name: author.name, user: author.user.name}].to_json
      expect(status).to be 200
    end
  end

  get "/v1/users/:user_id/authors/:id" do
    let(:user_id) { user.id }
    let(:id) { author.id }
    
    example "Listing specific author" do
      do_request

      expect(response_body).to eq({name: author.name, user: author.user.name }.to_json)
      expect(status).to be 200
    end
  end

  delete "/v1/users/:user_id/authors/:id" do
    let(:user_id) { user.id }
    let(:id) { author.id }
    
    example "Deleting a specific author" do
      do_request

      expect(status).to be 204
    end
  end

  put "/v1/users/:user_id/authors/:id" do
    parameter :name
    parameter :user_id

    let(:user_id) { user.id }
    let(:id) { author.id }
    
    example "Updating a specific author" do
      do_request(name: 'New name', user_id: user.id)

      expect(status).to be 204
    end
  end

  post "/v1/users/:user_id/authors" do
    let(:user_id) { user.id }
    
    parameter :name, required: true
    parameter :user_id, required: true

    example "Creating an author" do
      do_request(name: 'Foobar', user_id: user.id)
      
      expect(status).to be 201
    end
  end
end
