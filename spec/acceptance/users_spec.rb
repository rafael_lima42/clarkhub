require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource "Users" do
  header "Accept", "application/json"
  header "Authorization", :basic_auth

  let(:user) { create(:user, name: 'Rafael', role: 'user', username: 'rafael', password: 'password') }
  let(:basic_auth) do
    ActionController::HttpAuthentication::Basic.encode_credentials(user.username, user.password)
  end
  
  get "/v1/users/" do
    example "Listing users" do
      do_request

      expect(response_body).to eq [{name: user.name, username: user.username, role: user.role}].to_json
      expect(status).to be 200
    end
  end

  get "/v1/users/:id" do
    let(:id) { user.id }
    
    example "Listing specific user" do
      do_request

      expect(response_body).to eq({ name: user.name, username: user.username, role: user.role }.to_json)
      expect(status).to be 200
    end
  end

  delete "/v1/users/:id" do
    let(:id) { user.id }
    
    example "Deleting a specific user" do
      do_request

      expect(status).to be 204
    end
  end

  put "/v1/users/:id" do
    parameter :name
    parameter :username
    parameter :role

    let(:id) { user.id }
    
    example "Updating a specific user" do
      do_request(name: 'New name', username: 'newusername', role: 'guest')

      expect(status).to be 204
    end
  end

  post "/v1/users" do
    parameter :name, required: true
    parameter :username, required: true
    parameter :role, required: true
    parameter :password, required: true

    example "Creating an user" do
      do_request(name: 'Foo bar', username: 'foobar', role: 'admin', password: 'root')
      
      expect(status).to be 201
    end
  end
end
