require 'rails_helper'

describe Api::V1::AuthorsController do
  let(:user) { create(:user, :admin) }
  let!(:authors) { create_list(:author, 10) }
  let(:author) { authors.first }
  
  before do
    request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Basic.encode_credentials(user.username, user.password)
  end

  it { is_expected.to use_before_action(:authorize_user) }
  
  describe 'GET /v1/users/:user_id/authors' do
    let!(:author_for_pagination) { create(:author) }

    context 'without pagination param' do
      let(:expected_response) do
        authors.map do |author|
          { name: author.name, user: author.user.name }
        end
      end
          
      before { get :index, params: { user_id: author.user.id } }

      it 'returns authors' do
        json = JSON.parse(response.body, symbolize_names: true)
        expect(json).to eq expected_response
      end

      it { expect(response).to have_http_status(200) }
    end

    context 'with pagination param' do
      let(:expected_response) do
        [{ name: author_for_pagination.name, user: author_for_pagination.user.name }]
      end

      before { get :index, params: { user_id: author.user.id, page: 2 } }

      it 'returns authors' do
        json = JSON.parse(response.body, symbolize_names: true)
        expect(json).to eq expected_response
      end
    end
  end

  describe 'GET /v1/users/:user_id/authors/:id' do
    let(:expected_response) do
      { name: author.name, user: author.user.name }
    end

    before { get :show, params: { id: author.id, user_id: author.user.id } }

    context 'when the record exists' do
      it 'returns the author' do
        json = JSON.parse(response.body, symbolize_names: true)
        expect(json).to eq expected_response
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:author_id) { 100 }

      it { expect { get :show, params: { id: author_id, user_id: author.user.id } }.to raise_error(ActiveRecord::RecordNotFound) }
    end
  end

  describe 'DELETE /v1/users/:user_id/authors/:id' do
    before { delete :destroy, params: { id: author.id, user_id: author.user.id } }
    
    it 'destroys the author' do
      expect { author.reload }.to raise_error(ActiveRecord::RecordNotFound)
    end

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end

  describe 'PUT /v1/users/:user_id/authors/:id' do
    let(:valid_attributes) { { id: author.id, user_id: author.user.id, name: 'new_name' } }

    context 'when the record exists' do
      before { put :update, params: valid_attributes }

      it 'updates the record' do
        expect(author.reload.name).to eq valid_attributes[:name]
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  describe 'POST /v1/users/:user_id/authors' do
    let(:valid_attributes) { { name: 'John Doe', user_id: user.id } }

    context 'when the request is valid' do
      before { post :create, params:  valid_attributes }

      it 'creates an author' do
        json = JSON.parse(response.body)
        expect(json['name']).to eq('John Doe')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post :create, params: { user_id: author.user.id } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end
    end
  end
end
