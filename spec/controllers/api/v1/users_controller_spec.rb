require 'rails_helper'

describe Api::V1::UsersController do
  let!(:users) { create_list(:user, 10) }
  let(:user) { users.first }

  before do
    request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Basic.encode_credentials(user.username, user.password)
  end

  it { is_expected.to use_before_action(:authorize_user) }

  describe 'GET /users' do
    let!(:user_for_pagination) { create(:user) }

    context 'without pagination param' do
      let(:expected_response) do
        users.map do |user|
          { name: user.name, username: user.username, role: user.role }
        end
      end

      before { get :index }

      it 'returns users' do
        json = JSON.parse(response.body, symbolize_names: true)
        expect(json).to eq expected_response
      end

      it { expect(response).to have_http_status(200) }
    end

    context 'with pagination param' do
      let(:expected_response) do
        [{ name: user_for_pagination.name, username: user_for_pagination.username, role: user_for_pagination.role }]
      end

      before { get :index , params: { page: 2 } }

      it 'returns only the new user' do
        json = JSON.parse(response.body, symbolize_names: true)
        expect(json).to eq expected_response
      end
    end
  end

  describe 'GET /users/:id' do
    let(:expected_response) do
      { name: user.name, username: user.username, role: user.role }
    end

    before { get :show, params: { id: user.id } }

    context 'when the record exists' do
      it 'returns the user' do
        json = JSON.parse(response.body, symbolize_names: true)
        expect(json).to eq expected_response
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:user_id) { 100 }

      it { expect { get :show, params: { id: user_id } }.to raise_error(ActiveRecord::RecordNotFound) }
    end
  end

  describe 'DELETE /users/:id' do
    before { delete :destroy, params: { id: user.id } }

    it 'destroys the user' do
      expect { user.reload }.to raise_error(ActiveRecord::RecordNotFound)
    end

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end

  describe 'PUT /users/:id' do
    let(:valid_attributes) { { id: user.id, name: 'new_name', username: 'new_username', role: 'guest' } }

    context 'when the record exists' do
      before { put :update, params: valid_attributes }

      it 'updates the record' do
        expect(user.reload.name).to eq valid_attributes[:name]
        expect(user.reload.username).to eq valid_attributes[:username]
        expect(user.reload.role).to eq valid_attributes[:role]
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  describe 'POST /users' do
    let(:valid_attributes) { { name: 'John Doe', username: 'johndoe', password: 'NewPass' } }

    context 'when the request is valid' do
      before { post :create, params:  valid_attributes }

      it 'creates a user' do
        json = JSON.parse(response.body)
        expect(json['name']).to eq('John Doe')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      let(:invalid_attributes) { { name: 'Foo Bar', username: 'foobar' } }
      before { post :create, params: invalid_attributes }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end
    end
  end
end
