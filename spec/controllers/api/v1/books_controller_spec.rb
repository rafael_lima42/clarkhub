require 'rails_helper'

describe Api::V1::BooksController do
  let(:user) { create(:user, :admin) }
  let(:author) { create(:author, user: user) }
  let!(:books) { create_list(:book, 10) }
  let(:book) { books.first }
  
  before do
    request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Basic.encode_credentials(user.username, user.password)
    Timecop.freeze(Time.local(2017, 1, 1))
  end

  after { Timecop.return }

  it { is_expected.to use_before_action(:authorize_user) }
  
  describe 'GET /v1/users/:user_id/authors/:author_id/books' do
    let!(:book_for_pagination) { create(:book) }
    
    context 'without pagination param' do
      let(:expected_response) do
        books.map do |book|
          { title: book.title, author: book.author.name, published_at: book.published_at.strftime('%F') }
        end
      end
    
      before { get :index, params: { user_id: book.author.user.id, author_id: book.author.id } }

      it 'returns books' do
        json = JSON.parse(response.body, symbolize_names: true)
        expect(json).to eq expected_response
      end

      it { expect(response).to have_http_status(200) }
    end

    context 'with pagination param' do
      let(:expected_response) do
        [{ title: book_for_pagination.title, author: book_for_pagination.author.name, published_at: book_for_pagination.published_at.strftime('%F') }]
      end

      before { get :index , params: { user_id: book.author.user.id, author_id: book.author.id, page: 2 } }

      it 'returns only the new book' do
        json = JSON.parse(response.body, symbolize_names: true)
        expect(json).to eq expected_response
      end
    end
  end

  describe 'GET /v1/users/:user_id/authors/:author_id/books/:id' do
    let(:expected_response) do
      { title: book.title, author: author.name, published_at: book.published_at.strftime('%F') }
    end

    before { get :show, params: { id: book.id, user_id: book.author.user.id, author_id: book.author.id } }

    context 'when the record exists' do
      it 'returns the book' do
        json = JSON.parse(response.body, symbolize_names: true)
        expect(json).to eq expected_response
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:book_id) { 100 }

      it { expect { get :show, params: { id: book_id, user_id: book.author.user.id, author_id: book.author.id } }
             .to raise_error(ActiveRecord::RecordNotFound) }
    end
  end

  describe 'DELETE /v1/users/:user_id/authors/:author_id/books/:id' do
    before { delete :destroy, params: { id: book.id, user_id: book.author.user.id, author_id: book.author.id } }
    
    it 'destroys the book' do
      expect { book.reload }.to raise_error(ActiveRecord::RecordNotFound)
    end

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end

  describe 'PUT /v1/users/:user_id/authors/:author_id/books/:id' do
    let(:valid_attributes) { { id: book.id, user_id: book.author.user.id, author_id: book.author.id, title: 'new title' } }

    context 'when the record exists' do
      before { put :update, params: valid_attributes }

      it 'updates the record' do
        expect(book.reload.title).to eq valid_attributes[:title]
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  describe 'POST /v1/users/:user_id/authors/:author_id/books' do
    let(:valid_attributes) { { user_id: book.author.user.id, author_id: book.author.id,  title: 'Neverland', published_at: Time.current } }

    context 'when the request is valid' do
      before { post :create, params: valid_attributes }

      it 'creates a book' do
        json = JSON.parse(response.body)
        
        expect(json['title']).to eq('Neverland')
        expect(json['author']).to eq(author.name)
        expect(json['published_at']).to eq(Time.current.strftime('%F'))
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post :create, params: { user_id: book.author.user.id, author_id: book.author.id } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end
    end
  end
end
