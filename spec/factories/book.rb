FactoryGirl.define do
  factory :book do
    title 'Foobar adventures'
    published_at { Time.current }
    author { create(:author) }
  end
end
