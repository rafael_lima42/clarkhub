FactoryGirl.define do
  factory :author do
    name 'Foobar'
    user { create(:user) }
  end
end
