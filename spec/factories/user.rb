FactoryGirl.define do
  factory :user do
    name 'fake_name'
    username 'fake_username'
    password 'p4ssW0rd'
    role :user

    trait :guest do
      role :guest
    end

    trait :admin do
      role :admin
    end
  end
end
