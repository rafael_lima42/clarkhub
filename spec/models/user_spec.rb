require 'rails_helper'

describe User do
  it do
    is_expected.to define_enum_for(:role)
                     .with([:admin, :user, :guest])
  end

  it { is_expected.to have_secure_password }
  it { is_expected.to have_many(:authors) }
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:username) }
  it { is_expected.to validate_presence_of(:password_digest) }
end
