require 'rails_helper'

describe Author do
  it { is_expected.to belong_to(:user) }
  it { is_expected.to have_db_index(:user_id) }
  it { is_expected.to have_many(:books).dependent(:destroy) }
  it { is_expected.to validate_presence_of(:name) }
end
