require 'rails_helper'

describe Book do
  it { is_expected.to belong_to(:author) }
  it { is_expected.to have_db_index(:author_id) }
  it { is_expected.to validate_presence_of(:title) }
  it { is_expected.to validate_presence_of(:published_at) }
end
