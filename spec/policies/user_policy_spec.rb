require 'rails_helper'

describe UserPolicy do
  subject { UserPolicy.new(current_user, user) }

  let(:user) { build_stubbed(:user) }

  context 'role guest' do
    let(:current_user) { build_stubbed(:user, :guest) }

    it { should permit(:show) }
    it { should permit(:index) }

    it { should_not permit(:create)  }
    it { should_not permit(:update)  }
    it { should_not permit(:destroy) }
  end

  context 'role user' do
    let(:current_user) { build_stubbed(:user) }

    it { should permit(:show) }
    it { should permit(:index) }
    it { should permit(:create) }

    context 'when managing another user' do
      it { should_not permit(:update) }
      it { should_not permit(:destroy) }
    end

    context 'when managing his own record' do
      let(:current_user) { user }

      it { should permit(:update) }
      it { should permit(:destroy) }
    end
  end

  context 'admin user' do
    let(:current_user) { build_stubbed(:user, :admin) }

    it { should permit(:show) }
    it { should permit(:index) }
    it { should permit(:create)  }
    it { should permit(:update)  }
    it { should permit(:destroy) }
  end
end
