require 'rails_helper'

describe AuthorPolicy do
  subject { AuthorPolicy.new(user, author) }

  context 'when admin' do
    let(:user) { create(:user, :admin) }
    let(:author) { create(:author) }

    it { should permit(:show) }
    it { should permit(:index) }
    it { should permit(:create)  }
    it { should permit(:update)  }
    it { should permit(:destroy) }
  end

  context 'when role user' do
    context 'when managing his own records' do
      let(:user) { create(:user) }
      let(:author) { create(:author, user: user) }

      it { should permit(:show) }
      it { should permit(:index) }
      it { should permit(:create)  }
      it { should permit(:update)  }
      it { should permit(:destroy) }
    end

    context 'when managing other users records' do
      let(:user) { create(:user) }
      let(:author) { create(:author) }

      it { should permit(:show) }
      it { should permit(:index) }
      it { should permit(:create)  }
      
      it { should_not permit(:update)  }
      it { should_not permit(:destroy) }
    end
  end

  context 'when role guest' do
    context 'when he has created a record' do
      let(:user) { create(:user, :guest) }
      let(:author) { create(:author, user: user) }
    
      it { should permit(:show) }
      it { should permit(:index) }
      
      it { should_not permit(:create)  }
      it { should_not permit(:update)  }
      it { should_not permit(:destroy) }
    end
  end
end
