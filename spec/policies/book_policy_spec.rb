require 'rails_helper'

describe BookPolicy do
  subject { BookPolicy.new(user, book) }

  context 'when admin' do
    let(:user) { create(:user, :admin) }
    let(:book) { create(:book) }

    it { should permit(:show) }
    it { should permit(:index) }
    it { should permit(:create)  }
    it { should permit(:update)  }
    it { should permit(:destroy) }
  end

  context 'when role user' do
    context 'when managing his own records' do
      let(:user) { create(:user) }
      let(:author) { create(:author, user: user) }
      let(:book) { create(:book, author: author) }

      it { should permit(:show) }
      it { should permit(:index) }
      it { should permit(:create)  }
      it { should permit(:update)  }
      it { should permit(:destroy) }
    end

    context 'when managing other users records' do
      let(:user) { create(:user) }
      let(:author) { create(:author) }
      let(:book) { create(:book, author: author) }

      it { should permit(:show) }
      it { should permit(:index) }
      it { should permit(:create)  }
      
      it { should_not permit(:update)  }
      it { should_not permit(:destroy) }
    end
  end

  context 'when role guest' do
    context 'when he has created a record' do
      let(:user) { create(:user, :guest) }
      let(:author) { create(:author, user: user) }
      let(:book) { create(:book, author: author) }
    
      it { should permit(:show) }
      it { should permit(:index) }
      
      it { should_not permit(:create)  }
      it { should_not permit(:update)  }
      it { should_not permit(:destroy) }
    end
  end
end
