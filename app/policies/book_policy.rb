class BookPolicy
  attr_reader :user, :record
  
  def initialize(user, record)
    @user = user
    @record = record
  end

  def index?
    true
  end

  def show?
    true
  end

  def update?
    admin_or_same_user?
  end

  def destroy?
    admin_or_same_user?
  end

  def create?
    not_guest?
  end

  private
  
  def admin_or_same_user?
    @user.admin? || (@user.id == @record.author.user_id && not_guest?)
  end

  def not_guest?
    !@user.guest?
  end
end
