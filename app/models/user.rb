class User < ApplicationRecord
  has_many :authors
  
  enum role: [:admin, :user, :guest]

  has_secure_password

  validates_presence_of :name, :username, :password_digest
end
