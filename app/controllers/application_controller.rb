class ApplicationController < ActionController::API
  include ActionController::HttpAuthentication::Basic::ControllerMethods
  include Pundit

  attr_reader :current_user
  before_action :authenticate

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  private

  def authenticate
    authenticate_or_request_with_http_basic do |username, password|
      @current_user = User.find_by(username: username).try(:authenticate, password)
    end
  end

  def user_not_authorized
    head :forbidden
  end

  def authorize_user
    authorize current_user
  end
end
