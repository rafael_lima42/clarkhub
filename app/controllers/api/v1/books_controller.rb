class Api::V1::BooksController < ApplicationController
  before_action :set_book, only: [:show, :update, :destroy]
  before_action :authorize_user
  
  def index
    @books = Book.paginate(page: params[:page])
    render json: @books
  end

  def show
    render json: @book
  end

  def destroy
    @book.destroy
  end

  def update
    @book.update(book_params)
  end

  def create
    @book = Book.new(book_params)
    if @book.valid?
      @book.save!
      render json: @book, status: :created
    else
      render json: @book.errors, status: 422
    end
  end
  
  private

  def set_book
    @book = Book.find params[:id]
  end

  def book_params
    # whitelist params
    params.permit(:title, :author_id, :published_at)
  end
end
