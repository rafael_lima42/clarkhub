class Api::V1::AuthorsController < ApplicationController
  before_action :set_author, only: [:show, :update, :destroy]
  before_action :authorize_user
  
  def index
    @authors = Author.paginate(page: params[:page])
    render json: @authors
  end

  def show
    render json: @author
  end

  def destroy
    @author.destroy
  end

  def update
    @author.update(author_params)
  end

  def create
    @author = Author.new(author_params)
    if @author.valid?
      @author.save!
      render json: @author, status: :created
    else
      render json: @author.errors, status: 422
    end
  end
  
  private

  def set_author
    @author = Author.find params[:id]
  end

  def author_params
    # whitelist params
    params.permit(:name, :user_id)
  end
end
