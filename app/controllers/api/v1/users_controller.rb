class Api::V1::UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy]
  before_action :authorize_user
  
  def index
    @users = User.paginate(page: params[:page])
    render json: @users
  end

  def show
    render json: @user
  end

  def destroy
    @user.destroy
  end

  def update
    @user.update(user_params)
  end

  def create
    @user = User.new(user_params)
    if @user.valid?
      @user.save!
      render json: @user, status: :created
    else
      render json: @user.errors, status: 422
    end
  end

  private

  def set_user
    @user = User.find params[:id]
  end

  def user_params
    # whitelist params
    params.permit(:name, :username, :role, :password)
  end
end
