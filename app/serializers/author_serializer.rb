class AuthorSerializer < ActiveModel::Serializer
  attributes :name, :user

  def user
    object.user.name
  end
end
