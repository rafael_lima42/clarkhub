class UserSerializer < ActiveModel::Serializer
  attributes :name, :username, :role
end
