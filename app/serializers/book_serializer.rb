class BookSerializer < ActiveModel::Serializer
  attributes :title, :author, :published_at

  def author
    object.author.name
  end

  def published_at
    object.published_at.strftime('%F')
  end
end
