ActiveRecord::Base.transaction do
  p "Creating user for sample tests"
  User.create!(name: 'Rafael', username: 'rafael', password: 'password')
  
  p "Creating users...."
  10.times do
    User.create!(name: Faker::StarWars.character, username: Faker::Internet.user_name, password: Faker::Internet.password)
  end

  @users_ids = User.all.pluck(:id)

  p "Creating authors...."
  20.times do
    Author.create!(name: Faker::StarWars.droid, user_id: @users_ids.sample)
  end

  @authors_ids = Author.all.pluck(:id)
  
  p "Creating books...."
  30.times do
    Book.create!(title: Faker::Book.title, author_id: @authors_ids.sample, published_at: Faker::Date.forward(30))
  end

  p "All done!"
end
