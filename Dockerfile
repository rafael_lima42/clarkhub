FROM ruby:2.4.1
MAINTAINER rafaellima

RUN apt-get update && apt-get install -y \ 
  build-essential

RUN mkdir -p /app 
WORKDIR /app

COPY Gemfile Gemfile.lock ./ 
RUN gem install bundler && bundle install --jobs 20 --retry 5

# Copy the main application.
COPY . ./