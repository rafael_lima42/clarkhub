source 'https://rubygems.org'
ruby '~> 2.4.1'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

gem 'rails', '~> 5.1.3'
gem 'active_model_serializers', '~> 0.10.6'
gem 'pg', '~> 0.18.2'
gem 'pundit', '~> 1.1.0'

# Use Puma as the app server
gem 'puma', '~> 3.7'

# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'

# Use will_paginate as a pagination library
gem 'will_paginate', '~> 3.1.0'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'rb-readline'
  gem 'faker', '~> 1.8.4'
end

group :test do
  gem 'rspec-rails', '~> 3.5'
  gem 'shoulda-matchers', git: 'https://github.com/thoughtbot/shoulda-matchers.git', branch: 'rails-5'
  gem 'factory_girl_rails', '~> 4.8'
  gem 'database_cleaner', '~> 1.6.1'
  gem 'timecop', '~> 0.9.1'
end

group :test, :development do
  gem 'rspec_api_documentation', '~> 4.9.0'
end
