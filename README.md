# Clarkhub

An user role based API to manage authors and books records.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

```
$ ./bin/setup
```

### Prerequisites

You'll need to install `postgreSQL` and ruby `2.4.1` to run this project

### Installing

After running `bin/setup` all you'll need to do in run the rails server

```
./bin/rails server
```

### Docker

You can also use docker to run the server and test this app. In order to do that follow those instructions:

```
docker-compose build
docker-compose run app rake db:create
docker-compose run app rake db:migrate
docker-compose up
```

Docker will build the images, run the migrations, and finally run the rails server along postgres. You may also want to run `./bin/rails db:seed` and order commands
before starting the service.

## Usage

### Authentication

This project uses HTTP Basic Auth.

### Docs

Check the API on the /doc folder:

```
open doc/api/users/
open doc/api/authors/
open doc/api/books/
```

It's possible to use the gem [apitome](https://github.com/jejacks0n/apitome) to see the docs in the browser.

## Running the tests

Run `rspec spec`

### Tests overview

We basically have unit and integration tests. All the controllers have a suite that are testing the actions's response as well as the model and other services integrations. Models and services (like pundit for instance) were tested individually.

## Built With

* [rails-api](https://github.com/rails-api/rails-api) - The web framework used
* [postgreSQL](http://postgresql.org) - The database

## Authors

* ** Rafael Lima** - *Initial work* - [rafaellima](https://github.com/rafaellima)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
