Rails.application.routes.draw do
  scope module: 'api' do
    namespace :v1 do
      resources :users do
        resources :authors do
          resources :books
        end
      end
    end
  end
end
